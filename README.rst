|version| |wheel| |license| |supported-versions| |supported-implementations| |ci| |coverage|


.. |ci| image:: https://gitlab.com/dimapu/multi-expressions/badges/main/pipeline.svg
    :alt: Test status
    :target: https://gitlab.com/dimapu/multi-expressions/-/commits/main

.. |coverage| image:: https://gitlab.com/dimapu/multi-expressions/badges/main/coverage.svg
    :alt: Test coverage
    :target: https://gitlab.com/dimapu/multi-expressions/-/commits/main

.. |version| image:: https://badge.fury.io/py/multi-expressions.svg
    :alt: PyPI Package latest release
    :target: https://badge.fury.io/py/multi-expressions

.. |wheel| image:: https://img.shields.io/pypi/wheel/multi-expressions.svg
    :alt: PyPI Wheel
    :target: https://pypi.python.org/pypi/multi-expressions

.. |supported-versions| image:: https://img.shields.io/pypi/pyversions/multi-expressions.svg
    :alt: Supported versions
    :target: https://pypi.python.org/pypi/multi-expressions

.. |supported-implementations| image:: https://img.shields.io/pypi/implementation/multi-expressions.svg
    :alt: Supported implementations
    :target: https://pypi.python.org/pypi/multi-expressions

.. |license| image:: https://img.shields.io/gitlab/license/dimapu/multi-exceptions.svg?color=brightgreen
    :target: https://gitlab.com/dimapu/multi-exceptions/blob/main/LICENSE

=================
multi-expressions
=================

About multi-expressions
=======================

multi-expressions is a package implementing a custom exception class
which allows collecting multiple exceptions in a list or a dictionary.
This feature is useful, for example, when performing data or web-form
validation: instead of throwing one exception on the first error, all
errors can be collected and then presented to the user.

The code was taken and adapted from Django's ValidationError.

Installation
============

From pypi
---------

1. Install the package from pypi:

    `$ pip install multi-exceptions`


Documentation
=============
TBD.

Examples
========
TBD.

Tests
=====
TBD.

Changelog
=========
See `changelog`_.

Authors
=======
See `authors`_.

Contribution
============
See `contributing`_.

License
=======
multi-exceptions is licensed under the 3-clause BSD license. See `license`_.

Multi-exceptions package includes code from the Django package, which is
licensed under the three-clause BSD license, a permissive open source license.
The copyright note includes the DSF for compliance with Django's terms.


.. _changes: CHANGELOG
.. _authors: AUTHORS
.. _contributing: CONTRIBUTING
.. _license: LICENSE
