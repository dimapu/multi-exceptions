# Contributing

Contributions are accepted in the form of issues (e.g. for bug reports or 
feature requests) and merge requests.

## Issues
If you want to report a problem or request a new feature, you are encouraged
to open an issue on the
[New Issue](https://gitlab.com/dimapu/multi-exceptions/-/issues/new) page.

## Submitting code

To submit you code, you need to create a merge request.

### Via an issue and merge request
It is recommended to first create an issue with the description of the
intended feature or the problem you will be fixing. Then, on the issue page
of the gitlab web-interface, create a merge request to the `main` branch.
A corresponding branch will be created automatically.
Make your changes and push them to this branch. After your changes
are complete, remove "Draft" label from the merge request name - this is
an indication that the request is ready for review.

### Via a merge request
If your problem is simple, you can skip creating an issue.

Do the following:
* Go to [gitlab.com/dimapu/multi-exceptions](https://gitlab.com/dimapu/multi-exceptions).
* Press *+* (Create new...) -> *This repository / New Branch*.
* Name you branch. E.g. `fix-docs-formatting` or `add-feature`.
* Choose `main` in *Create from* field.
* Press *Create branch*.

Now you can edit code through the website, or push changes to this branch
from your computer.

### Editing code in the merge request
You can work with the code either through the [gitlab.com](https://gitlab.com)
web interface, or clone the repository locally to your computer and use
git's functionality to interact with the repository (recommended).

For editing `multi-exceptions` via the web-interface, use
[this link](https://gitlab.com/-/ide/project/dimapu/multi-exceptions/edit/main/-/).
Make sure you commit your changes to the correct branch, as you should not 
commit the changes directly to the default branch (i.e. `main` branch).


## Formatting

### Code formatting

Use `black` and `isort` on your code to format it properly.

### Documentation: section structure

We use the following symbols for the documentation sections:
```
# with overline, for parts
* with overline, for chapters
=, for sections
-, for subsections
^, for subsubsections
", for paragraphs
```

### Class method order

1. Dunder methods
2. Abstract methods (interface description)
3. Properties
4. Public methods
5. Private methods
